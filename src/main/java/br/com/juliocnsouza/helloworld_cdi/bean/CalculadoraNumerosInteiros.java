/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.helloworld_cdi.bean;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.inject.Qualifier;

/**
 *
 * @author juliocnsouza
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface CalculadoraNumerosInteiros {
}
