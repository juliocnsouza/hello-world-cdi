/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.helloworld_cdi.bean;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author juliocnsouza
 */
@Stateless
public class ContadorCaracteres {

    @PostConstruct
    public void init() {
        System.out.println("-> pronto " + this.getClass());
    }

    public int contador(String str) {
        if (str == null || str.isEmpty()) {
            return 0;
        }
        else {
            return str.length();
        }
    }
}
