/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.helloworld_cdi.bean;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author juliocnsouza
 */
@Stateless
public class Mensagem {

    public static final String MSG = "Olá!!!! esta msg tem ";
    @Inject
    private ContadorCaracteres contador;

    @PostConstruct
    public void init() {
        System.out.println("-> pronto " + this.getClass());
    }

    public String getMessage() {
        System.out.println("-> retornando mensagem do metodo getMessage()");
        return MSG + contador.contador(MSG) + " caracteres";
    }
}
